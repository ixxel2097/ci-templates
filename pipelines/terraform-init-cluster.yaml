---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - jobs/checkov-scan.yaml
  - jobs/terraform-fmt.yaml
  - jobs/terraform-tflint.yaml
  - jobs/git-simple-push.yaml
  - jobs/terraform-init.yaml
  - jobs/terraform-validate.yaml
  - jobs/terraform-plan.yaml
  - jobs/terraform-apply.yaml
  - jobs/terraform-destroy.yaml
  - jobs/terraform-output.yaml
  - jobs/generate-child-pipeline.yaml
  - jobs/git-clone.yaml
  - snippets/proxy.yaml
  - snippets/images.yaml
  - snippets/global-parameters.yaml

workflow:
  rules:
  - if: $CI_MERGE_REQUEST_IID
  - if: $CI_COMMIT_BRANCH
  - if: $CI_COMMIT_TAG

variables:
  TF_ROOT: ${CI_PROJECT_DIR}                                                                             # The relative path to the root directory of the Terraform project
  TF_USERNAME: gitlab-ci-token                                                                           # https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#get-started-using-gitlab-ci
  TF_PASSWORD: ${GITLAB_TOKEN}                                                                           # The project access token as a maintainer
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}              # The state address in gitlab
  GITLAB_URL: gitlab.ratp.net                                                                            # The url of gitlab without https://
  GIT_CSI_PROVIDER_URL: https://gitlab.ratp.net/nubes/pcs/init-cluster/init-csi-secret-provider-class.git
  GIT_CLONE_DIR_CSI_PROVIDER: csi-provider
  TF_DESTROY:
    value: 'false'
    description: 'This variable allows terraform destroy. Default: False, set to true to destroy current infra'

.destroy_rule:
  rules:
  - if: $TF_DESTROY == "true"

.create_rule:
  rules:
  - if: $TF_DESTROY == "false"

stages:
- prepare
- init
- validate
- build
- deploy
- destroy
- output
- prepare-csi-provider
- generation
- bootstrap-apps
- sync

checkov-scan:
  stage: prepare
  extends:
  - .pipeline_env
  - .checkov-scan
  - .proxy_ratp

terraform-tflint:
  stage: prepare
  extends:
  - .pipeline_env
  - .terraform-tflint

terraform-fmt:
  stage: prepare
  extends:
  - .pipeline_env
  - .terraform-fmt
  needs:
  - job: terraform-tflint
    artifacts: true

git-push-terraform:
  stage: prepare
  extends:
  - .pipeline_env
  - .git-simple-push
  needs:
  - job: terraform-fmt
    artifacts: true
  - job: checkov-scan
  variables:
    GIT_TOKEN: ${GITLAB_TOKEN}
    GIT_WORKDIR: .
    REPO_URL: ${CI_PROJECT_URL}
    REPO_BRANCH: ${CI_COMMIT_BRANCH}
    GIT_MSG: CI/CD trigger from TERRAFORM REPO ${CI_PROJECT_NAME}. User ${GITLAB_USER_NAME} commit ${CI_COMMIT_SHORT_SHA}

terraform-init:
  stage: init
  extends:
  - .pipeline_env
  - .terraform-init
  - .proxy_ratp

terraform-validate:
  stage: validate
  extends:
  - .pipeline_env
  - .terraform-validate
  dependencies:
  - terraform-init

terraform-plan:
  stage: build
  extends:
  - .pipeline_env
  - .terraform-plan
  - .create_rule
  - .proxy_ratp
  dependencies:
  - terraform-init

terraform-apply:
  stage: deploy
  allow_failure: false
  extends:
  - .pipeline_env
  - .terraform-apply
  - .create_rule
  - .proxy_ratp
  dependencies:
  - terraform-init
  - terraform-plan
  when: manual

terraform-plan-destroy:
  stage: build
  extends:
  - .pipeline_env
  - .terraform-plan
  - .destroy_rule
  - .proxy_ratp
  dependencies:
  - terraform-init

terraform-destroy:
  stage: destroy
  extends:
  - .pipeline_env
  - .terraform-destroy
  - .destroy_rule
  - .proxy_ratp
  dependencies:
  - terraform-init
  - terraform-plan
  when: manual

terraform-output:
  stage: output
  allow_failure: false
  extends:
  - .pipeline_env
  - .terraform-output
  - .create_rule
  - .proxy_ratp
  dependencies:
  - terraform-init
  - terraform-plan

clone-csi-provider-repo:
  stage: prepare-csi-provider
  extends:
  - .pipeline_env
  - .git-clone
  - .create_rule
  variables:
    GIT_TOKEN: ${GITLAB_TOKEN}
    REPO_URL: ${GIT_CSI_PROVIDER_URL}
    REPO_BRANCH: main
    CLONE_DIR: ${GIT_CLONE_DIR_CSI_PROVIDER}

inject-values:
  stage: prepare-csi-provider
  extends:
  - .pipeline_env
  - .cd-image
  - .create_rule
  needs:
  - job: clone-csi-provider-repo
  - job: terraform-output
  dependencies:
  - clone-csi-provider-repo
  - terraform-output
  variables:
    VALUE_WORKINGDIR: ${GIT_CLONE_DIR_CSI_PROVIDER}
  script:
  - |-
    # shellcheck disable=SC2154
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"
    set +e
    echo -e "${YELLOW}[ EXECUTING ] > Preparing secret provider class values.yaml for cluster environment ${ENV}${BLANK}"
    ls -la ${VALUE_WORKINGDIR}/${ENV}
    yq eval ".csi-secret-provider-class.spec.objectName = \"${objectName}\"" -i ${VALUE_WORKINGDIR}/${ENV}/values.yaml
    yq eval ".csi-secret-provider-class.parameters.userAssignedIdentityID = \"${userAssignedIdentityID}\"" -i ${VALUE_WORKINGDIR}/${ENV}/values.yaml
    yq eval ".csi-secret-provider-class.parameters.keyvaultName = \"${keyvaultName}\"" -i ${VALUE_WORKINGDIR}/${ENV}/values.yaml
    yq eval ".csi-secret-provider-class.parameters.tenantId = \"${tenantId}\"" -i ${VALUE_WORKINGDIR}/${ENV}/values.yaml
    yq -C ${VALUE_WORKINGDIR}/${ENV}/values.yaml
  artifacts:
    paths:
    - ${VALUE_WORKINGDIR}
    expire_in: 10 mins

git-push-csi-provider-repo:
  stage: prepare-csi-provider
  extends:
  - .pipeline_env
  - .git-simple-push
  - .create_rule
  needs:
  - job: inject-values
  dependencies:
  - inject-values
  variables:
    GIT_TOKEN: ${GITLAB_TOKEN}
    REPO_URL: ${GIT_CSI_PROVIDER_URL}
    REPO_BRANCH: main
    GIT_WORKDIR: ${GIT_CLONE_DIR_CSI_PROVIDER}
    GIT_MSG: CI/CD trigger from REPO ${CI_PROJECT_PATH}. User ${GITLAB_USER_NAME} branch ${CI_COMMIT_REF_NAME} commit ${CI_COMMIT_SHORT_SHA}

generate-child-pipeline:
  stage: generation
  extends:
  - .generate-child-pipeline
  - .create_rule
  variables:
    SCRIPT_PATH: ${GENERATION_SCRIPT_PATH}

provision-base-apps:
  stage: bootstrap-apps
  variables:
    APP_CLUSTER: ${ARGOCD_CLUSTER_TF}
    ENV: ${ENV}
  extends:
  - .create_rule
  trigger:
    include:
    - artifact: child-pipeline-gitlab-ci.yml
      job: generate-child-pipeline
    strategy: depend
