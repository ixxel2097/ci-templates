---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.git-merge:
  extends:
  - .ci-image
  variables:
    GIT_TOKEN: ''
    REPO_URL: ''
    REPO_TARGET_BRANCH: main
    REPO_SOURCE_BRANCH: pipeline
    GIT_WORKDIR: .
    GIT_MSG: ''
    TLS_VERIFY: 'false'
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"

    echo -e "${BLUE}[ STEP - CLONING GIT REPO ] > Cloning repo ${REPO_URL}${BLANK}"
    echo -e "${CYAN}[ INFO ] > Git version.${BLANK}"
    git version

    echo -e "${CYAN}[ INFO ] > Listing current directory content${BLANK}"
    ls -la

    [[ ${REPO_URL} == *.git ]] || REPO_URL+=.git

    echo -e "${YELLOW}[ EXECUTING ] > Merging to the repo ${REPO_URL} branch [${REPO_TARGET_BRANCH}].${BLANK}"
    if [[ -z ${GIT_TOKEN} ]]
    then
      echo -e "${CYAN}[ INFO ] > No token provided, pushing without authentication.${BLANK}"
      PUSH_URL=${REPO_URL}
    else
      echo -e "${CYAN}[ INFO ] > Token has been provided, pushing with authentication.${BLANK}"
      PUSH_URL=$(echo "${REPO_URL}" | sed "s/https:\/\//&gitlab-ci-token:${GIT_TOKEN}@/")
    fi
    echo -e "${CYAN}[ INFO ] > Reworked URL --> ${PUSH_URL}.${BLANK}"
  script:
  - |-
    set +e

    echo -e "${YELLOW}[ EXECUTING ] > setting up git config to mail ${GITLAB_USER_EMAIL} and username ${GITLAB_USER_NAME}.${BLANK}"
    git config --global --add safe.directory "$(realpath "${GIT_WORKDIR}")" >/dev/null 2>&1
    git config --global user.email "${GITLAB_USER_EMAIL}" >/dev/null 2>&1
    git config --global user.name "${GITLAB_USER_NAME}" >/dev/null 2>&1
    git config --global http.sslVerify ${TLS_VERIFY} >/dev/null 2>&1
    git -C ${GIT_WORKDIR} config --list

    echo -e "${CYAN}[ INFO ] > git status.${BLANK}"
    git -C ${GIT_WORKDIR} -c color.status=always status
    echo -e "${CYAN}[ INFO ] > git diff.${BLANK}"
    git -C ${GIT_WORKDIR} diff --color=always
    echo -e "${CYAN}[ INFO ] > git branch.${BLANK}"
    git -C ${GIT_WORKDIR} branch
    echo -e "${CYAN}[ INFO ] > git origin.${BLANK}"
    git -C ${GIT_WORKDIR} remote show origin

    echo -e "${YELLOW}[ EXECUTING ] > Fetching branches because of shallow clone${BLANK}"
    git -C ${GIT_WORKDIR} remote set-branches origin ${REPO_TARGET_BRANCH}
    git -C ${GIT_WORKDIR} fetch -v origin ${REPO_TARGET_BRANCH}

    echo -e "${YELLOW}[ EXECUTING ] > Commit and push changes to ${REPO_SOURCE_BRANCH} before merge${BLANK}"
    git -C ${GIT_WORKDIR} add --all
    git -C ${GIT_WORKDIR} commit -m "${GIT_MSG}"
    git -C ${GIT_WORKDIR} remote set-url --push origin "${PUSH_URL}"
    git -C ${GIT_WORKDIR} push origin HEAD:${REPO_SOURCE_BRANCH}

    echo -e "${YELLOW}[ EXECUTING ] > Checkout to ${PURPLE}${REPO_TARGET_BRANCH}${YELLOW} and merging from branch ${PURPLE}${REPO_SOURCE_BRANCH}${YELLOW} on ${PUSH_URL}${BLANK}"
    git -C ${GIT_WORKDIR} checkout ${REPO_TARGET_BRANCH}
    # git -C ${GIT_WORKDIR} remote set-url --push origin "${PUSH_URL}"
    git -C ${GIT_WORKDIR} merge ${REPO_SOURCE_BRANCH}
    git -C ${GIT_WORKDIR} push origin HEAD:${REPO_TARGET_BRANCH}

    if [[ $? = 0 ]]
    then
      echo -e "${GREEN}[ SUCCESS ] > Git commit/push succeeded.${BLANK}"
    else
      echo -e "${RED}[ FAIL ] > Git commit/push failed!.${BLANK}"
      exit 1
    fi
