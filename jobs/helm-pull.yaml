---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.helm-pull:
  extends:
  - .cd-image
  variables:
    HELM_REPO_NAME: default
    HELM_REPO_URL: ''
    HELM_REPO_USER: '""'
    HELM_REPO_PWD: '""'
    CHART_NAME: ''
    CHART_VERSION: '""'
    UNRELEASED_VERSION: ''
    LATEST: 'false'
    UNTAR: 'false'
    DEVEL: 'false'
    DESTINATION: pkg
    WORKINGDIR: .
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"

    echo -e "${BLUE}[ STEP - Helm pull ] > Pulling Helm chart${BLANK}"
    echo -e "${CYAN}[ INFO ] > Helm version.${BLANK}"
    helm version --short

    mkdir -p ${DESTINATION}

    echo -e "${YELLOW}[ EXECUTING ] > Adding repo named ${HELM_REPO_NAME} from ${HELM_REPO_URL}.${BLANK}"
    helm repo add ${HELM_REPO_NAME} ${HELM_REPO_URL} --force-update \
                                                     --pass-credentials \
                                                     --username ${HELM_REPO_USER} \
                                                     --password ${HELM_REPO_PWD} \
                                                     --insecure-skip-tls-verify

    if [[ $? = 0 ]]
    then
      echo -e "${GREEN}[ SUCCESS ] > Helm repo added successfully !${BLANK}"
    else
      echo -e "${RED}[ ERROR ] > Helm repo failed to be added.${BLANK}"
      exit 1
    fi

    OPTIONS=""
    DEVEL_OPTION=""
    if [[ ${UNTAR} = "true" ]]
    then
      echo -e "${CYAN}[ INFO ] > Using option --untar.${BLANK}"
      OPTIONS+=" --untar"
    fi
    if [[ ${DEVEL} = "true" ]]
    then
      echo -e "${CYAN}[ INFO ] > Using option --devel.${BLANK}"
      OPTIONS+=" --devel"
      DEVEL_OPTION=" --devel"
    fi

    if [[ ${LATEST} = "true" ]]
    then
      echo -e "${YELLOW}[ EXECUTING ] > Fetching latest version.${BLANK}"
      LAST_CHART_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --versions ${DEVEL_OPTION} --output json | jq -r '.[].version' | sort -V | tail -1)
      LAST_APP_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --version ${LAST_CHART_VERSION} ${DEVEL_OPTION} --output json | jq -r '.[].app_version')
      # LAST_APP_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --versions --output json | jq -r '.[].app_version' | sort -V | tail -1)
      echo -e "${CYAN}[ INFO ] > Last version for chart ${YELLOW}${HELM_REPO_NAME}/${CHART_NAME}${CYAN} is ${YELLOW}${LAST_CHART_VERSION}${CYAN} and appVersion ${YELLOW}${LAST_APP_VERSION}${BLANK}"
      CHART_VERSION=${LAST_CHART_VERSION}
      echo "CHART_VERSION=${LAST_CHART_VERSION}" >> helm-pull.env
      echo "CHART_APP_VERSION=${LAST_APP_VERSION}" >> helm-pull.env
    elif [[ -n "${UNRELEASED_VERSION}" ]]
    then
      echo -e "${YELLOW}[ EXECUTING ] > Fetching unreleased version ${UNRELEASED_VERSION}${BLANK}"
      UNRELEASED_CHART_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --versions ${DEVEL_OPTION} --output json | jq -r ".[] | select(.version|test(\".${UNRELEASED_VERSION}\")) | .version" | sort -V | tail -1)
      echo -e "${CYAN}[ INFO ] > Unreleased chart version found --> ${PURPLE}${UNRELEASED_CHART_VERSION}${BLANK}"
      UNRELEASED_APP_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --version ${UNRELEASED_CHART_VERSION} ${DEVEL_OPTION} --output json | jq -r '.[].app_version')
      echo -e "${CYAN}[ INFO ] > Unreleased version for chart ${YELLOW}${HELM_REPO_NAME}/${CHART_NAME}${CYAN} is ${YELLOW}${UNRELEASED_CHART_VERSION}${CYAN} and appVersion ${YELLOW}${UNRELEASED_APP_VERSION}${BLANK}"
      CHART_VERSION=${UNRELEASED_CHART_VERSION}
      echo "CHART_VERSION=${UNRELEASED_CHART_VERSION}" >> helm-pull.env
      echo "CHART_APP_VERSION=${UNRELEASED_APP_VERSION}" >> helm-pull.env
    elif [[ -n "${SPECIFIC_APPVERSION}" ]]
    then
      echo -e "${YELLOW}[ EXECUTING ] > Fetching Helm chart from specific appVersion ${PURPLE}${SPECIFIC_APPVERSION}${BLANK}"
      SPECIFIC_CHART_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --versions ${DEVEL_OPTION} --output json | jq -r ".[] | select(.app_version|test(\"${SPECIFIC_APPVERSION}\")) | .version" | sort -V | tail -1)
      echo -e "${CYAN}[ INFO ] > Specific chart version found --> ${PURPLE}${SPECIFIC_CHART_VERSION}${BLANK}"
      CHART_VERSION=${SPECIFIC_CHART_VERSION}
      CHART_APP_VERSION=${SPECIFIC_APPVERSION}
      echo "CHART_VERSION=${SPECIFIC_CHART_VERSION}" >> helm-pull.env
      echo "CHART_APP_VERSION=${SPECIFIC_APPVERSION}" >> helm-pull.env
    else
      echo -e "${YELLOW}[ EXECUTING ] > Static version pulling${BLANK}"
      APP_VERSION=$(helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --version ${CHART_VERSION} ${DEVEL_OPTION} --output json | jq -r '.[].app_version')
      echo -e "${CYAN}[ INFO ] > Fetching version ${YELLOW}${CHART_VERSION}${CYAN} and appVersion ${YELLOW}${APP_VERSION}${CYAN} for chart ${YELLOW}${HELM_REPO_NAME}/${CHART_NAME}${BLANK}"
      echo "CHART_VERSION=${CHART_VERSION}" >> helm-pull.env
      echo "CHART_APP_VERSION=${APP_VERSION}" >> helm-pull.env
    fi
  script:
  - |-
    set +e
    echo -e "${CYAN}[ INFO ] > Listing versions of chart${BLANK}"
    helm search repo ${HELM_REPO_NAME}/${CHART_NAME} --versions ${DEVEL_OPTION}

    echo -e "${YELLOW}[ EXECUTING ] > Pulling chart ${PURPLE}${CHART_NAME}${YELLOW} version ${PURPLE}${CHART_VERSION}${YELLOW} from ${HELM_REPO_URL} in ${DESTINATION}${BLANK}"
    rm -rf ${DESTINATION:?}/${CHART_NAME:?}
    helm pull ${HELM_REPO_NAME}/${CHART_NAME} --version ${CHART_VERSION} \
                                              --destination ${DESTINATION} \
                                              --insecure-skip-tls-verify \
                                              ${OPTIONS}

    if [[ $? = 0 ]]
    then
      PULLED_PKG=$(find ${DESTINATION} -maxdepth 1 -type f -regex ".*\.tgz" | awk -F '/' '{print $NF}')
      PULL_PKG_PATH=$(realpath ${DESTINATION}/${PULLED_PKG})
      echo "PULLED_PKG=${PULLED_PKG}" >> helm-pull.env
      echo "PULLED_PKG_PATH=${PULL_PKG_PATH}" >> helm-pull.env
      echo "PULLED_PKG_DIR=${DESTINATION}" >> helm-pull.env
      echo -e "${CYAN}[ INFO ] > Package [[ ${YELLOW}${PULLED_PKG}${CYAN} ]] pulled${BLANK}"
      ls -la --color ${DESTINATION}
      echo -e "${GREEN}[ SUCCESS ] > Helm pull succeeded without any error.${BLANK}"
    else
      echo -e "${RED}[ ERROR ] > Helm pull didn't succeed !${BLANK}"
      exit 1
    fi
  artifacts:
    reports:
      dotenv: helm-pull.env
    paths:
    - ${DESTINATION}
    expire_in: 15 mins
