---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.set_project_version:
  extends:
  - .ci-image
  stage: .pre
  variables:
    TLS_VERIFY: 'false'
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"
  script:
  - |-
    set +e
    if [[ "$CI_COMMIT_REF_NAME" =~ ^(release\/)([0-9]+\.[0-9]+)$ ]]
    then
      echo -e "${YELLOW}[ EXECUTING ] > setting up git config to mail ${GITLAB_USER_EMAIL} and username ${GITLAB_USER_NAME}.${BLANK}"
      git config --global --add safe.directory $CI_PROJECT_DIR
      git config --global user.email "${GITLAB_USER_EMAIL}"
      git config --global user.name "${GITLAB_USER_NAME}"
      git config --global http.sslVerify ${TLS_VERIFY}
      git config --list

      git fetch --all --tags --force
      echo -e "${CYAN}[ INFO ] Checkout track ${PURPLE}main${BLANK}"
      git checkout --track origin/main
      echo -e "${CYAN}[ INFO ] Checkout track ${PURPLE}$CI_COMMIT_BRANCH${BLANK}"
      git checkout --track origin/$CI_COMMIT_BRANCH

      # Defaulting to maj.min.0
      proj_version="v${BASH_REMATCH[2]}.0"

      # Looking for previous tag
      echo -e "${CYAN}[ INFO ] Git merge-base with ${PURPLE}main${CYAN} and ${PURPLE}$CI_COMMIT_BRANCH ${CYAN}${BLANK}"
      git merge-base main $CI_COMMIT_BRANCH
      branch_tail=$(git merge-base main $CI_COMMIT_BRANCH)
      last_tag=''

      # For each commit of the branch
      echo -e "${CYAN}[ INFO ] Analyzing each commit for tag${BLANK}"
      git cherry $branch_tail -v

      cherry=$(git cherry $branch_tail | cut -d' ' -f2)
      cherry="$branch_tail $cherry"
      for commit in $cherry
      do
        tmp_tag=$(git tag --points-at $commit)
        if [ "$tmp_tag" != '' ]
        then
          if [ "$CI_COMMIT_SHA" != "$commit" ]
          then
            last_tag="$tmp_tag"
            echo -e "${CYAN}[ INFO ] Tag ${YELLOW}$last_tag${CYAN} found at commit ${YELLOW}$commit${BLANK}"
          else
            echo -e "${RED}[ ERROR ] Your current commit ${PURPLE}$CI_COMMIT_SHA${RED} is actually your last tag ${PURPLE}$tmp_tag${RED}. Cannot perform tag on a commit already tagged.${BLANK}"
            exit 1
          fi
        fi
      done

      if [ "$last_tag" != '' ]
      then
        # Found previous tag, increment patch digit
        if ! [[ "$last_tag" =~ ^v?([0-9]+\.[0-9]+\.)([0-9]+)$ ]]
        then
          echo -e "${RED}[ ERROR ] > Last tag ${PURPLE}$last_tag${RED} is not in Semantic Versioning (vMaj.Min.Patch ie: v1.0.0). Please perform manual correction${BLANK}"
          exit 1
        else
          proj_version="v${BASH_REMATCH[1]}$(( ${BASH_REMATCH[2]} + 1 ))"
        fi
      fi
    else
      proj_version="unreleased-$CI_COMMIT_SHORT_SHA"
      proj_short_version="$CI_COMMIT_SHORT_SHA"
    fi
    echo -e "${CYAN}[ INFO ] Using ${PURPLE}$proj_version${CYAN} for project version${BLANK}"
    echo "PROJECT_VERSION=$proj_version" >> set_project_version.env
    echo "PROJECT_SHORT_VERSION=$proj_short_version" >> set_project_version.env
  artifacts:
    reports:
      dotenv: set_project_version.env
