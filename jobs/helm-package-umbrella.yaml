---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.helm-package-umbrella:
  extends:
  - .ci-image
  variables:
    UPDATE_APP_NAME: ''
    UPDATE_APP_TAG: ''
    UMBRELLA_CHART_NAME: ''
    UMBRELLA_CHART_VERSION: ''
    UMBRELLA_CHART_APP_VERSION: ''
    VALUES_FILE: values.yaml
    DESTINATION: helm_package_dir
    INCREMENT: patch
    UMBRELLA_DIR: umbrella
    SUBDIR: chart
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"

    echo -e "${BLUE}[ STEP - Helm-chart linting ] > Linting Helm chart.${BLANK}"
    echo -e "${CYAN}[ INFO ] > Helm version.${BLANK}"
    helm version --short
  script:
  - |-
    set +e

    echo -e "${YELLOW}[ EXECUTING ] > Packaging helm chart ${UMBRELLA_DIR}.${BLANK}"
    if [[ -d ${UMBRELLA_DIR} ]]
    then
      echo -e "${GREEN}[ CHECK SUCCESS ] > Directory exists, proceeding packaging.${BLANK}"
    else
      echo -e "${RED}[ CHECK ERROR ] > Directory doesn't exist, canceling packaging.${BLANK}"
      exit 1
    fi
    yq e ${UMBRELLA_DIR}/${SUBDIR}/Chart.yaml -C
    UMBRELLA_CHART_NAME=$(yq e '.name' ${UMBRELLA_DIR}/${SUBDIR}/Chart.yaml)
    UMBRELLA_CURRENT_VERSION=$(yq e '.version' ${UMBRELLA_DIR}/${SUBDIR}/Chart.yaml)
    UMBRELLA_CURRENT_APP_VERSION=$(yq e '.appVersion' ${UMBRELLA_DIR}/${SUBDIR}/Chart.yaml)

    UMBRELLA_CHART_VERSION=$(semver bump ${INCREMENT} ${UMBRELLA_CURRENT_VERSION})
    UMBRELLA_CHART_APP_VERSION=${UMBRELLA_CURRENT_APP_VERSION}
    echo -e "${CYAN}[ INFO ] > Bumping umbrella chart version from, ${PURPLE}${UMBRELLA_CURRENT_VERSION}${CYAN} to ${PURPLE}${UMBRELLA_CHART_VERSION}.${BLANK}"

    echo -e "${CYAN}[ INFO ] > The umbrella helm chart ${YELLOW}${UMBRELLA_CHART_NAME}${CYAN} will be packaged as version ${YELLOW}${UMBRELLA_CHART_VERSION}${CYAN} and appVersion ${YELLOW}${UMBRELLA_CHART_APP_VERSION}${CYAN} on directory target ${YELLOW}${DESTINATION}${BLANK}"
    echo -e "${CYAN}[ INFO ] > The application updated is ${YELLOW}${UPDATE_APP_NAME}${CYAN} and bump is ${YELLOW}${UPDATE_APP_TAG}${BLANK}"
    echo -e "${CYAN}[ INFO ] > The associated chart updated is ${YELLOW}${UPDATE_APP_NAME}${CYAN} and bump is ${YELLOW}${CHART_VERSION}${BLANK}"
    yq e ".\"${UPDATE_APP_NAME}\".image.tag = \"${UPDATE_APP_TAG}\"" -i ${UMBRELLA_DIR}/${SUBDIR}/${VALUES_FILE}
    yq e ".\"${UPDATE_APP_NAME}\".image.tag = \"${UPDATE_APP_TAG}\"" -i ${UMBRELLA_DIR}/${SUBDIR}/values-template.yaml
    yq e "(.dependencies[] | select(.name == \"${UPDATE_APP_NAME}\")).version = \"${CHART_VERSION}\"" -i ${UMBRELLA_DIR}/${SUBDIR}/Chart.yaml
    helm dependency build ${UMBRELLA_DIR}/${SUBDIR}/
    PKG_OUTPUT=$(helm package ${UMBRELLA_DIR}/${SUBDIR}/ --destination=${DESTINATION} \
                                            --version=${UMBRELLA_CHART_VERSION} \
                                            --app-version=${UMBRELLA_CHART_APP_VERSION})

    if [[ $? = 0 ]]
    then
      PKG_NAME=$(echo "${PKG_OUTPUT}" | awk -F '/' '{print $NF}')
      PKG_PATH=$(realpath ${DESTINATION}/${PKG_NAME})
      echo "UMBRELLA_CHART_NAME=${UMBRELLA_CHART_NAME}" >> helm-package-umbrella.env
      echo "UMBRELLA_CHART_VERSION=${UMBRELLA_CHART_VERSION}" >> helm-package-umbrella.env
      echo "UMBRELLA_CHART_APP_VERSION=${UMBRELLA_CHART_APP_VERSION}" >> helm-package-umbrella.env
      echo "PKG_NAME=${PKG_NAME}" >> helm-package-umbrella.env
      echo "PKG_PATH=${PKG_PATH}" >> helm-package-umbrella.env
      echo "PKG_DIR=${DESTINATION}" >> helm-package-umbrella.env
      echo -e "${CYAN}[ INFO ] > Package [[ ${YELLOW}${PKG_NAME}${CYAN} ]] created --> ${PKG_OUTPUT}${BLANK}"
      ls -la --color ${DESTINATION}
      echo -e "${GREEN}[ SUCCESS ] > Helm packaging succeeded without any error.${BLANK}"
    else
      echo -e "${RED}[ ERROR ] > Helm packaging didn't succeed !${BLANK}"
      exit 1
    fi
  artifacts:
    reports:
      dotenv: helm-package-umbrella.env
    paths:
    - ${DESTINATION}/*.tgz
    expire_in: 15 mins
