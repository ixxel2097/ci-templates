---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.buildah-bud:
  extends:
  - .cd-image
  variables:
    IMAGE_NAME: ''                          # Image name
    TAG: ''                                 # Image tag
    FILE: Dockerfile
    FORMAT_BUILD: docker
    BUILD_ARG: ''
    VERIFY_TLS: 'false'
    COMPRESS: 'true'
    COMPRESSION_FORMAT: docker-archive
    TARGET_DIR: artefacts                   # directory where the tarball is created
    GITHUB_API_AUTH: 'false'
    GITHUB_API_REST_TOKEN: ''
    WORKINGDIR: .
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"
    echo -e "${BLUE}[ STEP - BUILDING IMAGE ] > Building docker image ${IMAGE_NAME}:${TAG} with buildah.${BLANK}"
    echo -e "${CYAN}[ INFO ] > Buildah version.${BLANK}"
    buildah --version
    echo -e "${CYAN}[ INFO ] > Podman version.${BLANK}"
    podman --version

    if [ "${BUILD_ARG}" ]
    then
        [[ ! "$BUILD_ARG" =~ ^\|.+ ]] || BUILD_ARG="${BUILD_ARG:1}"
        full_str=${BUILD_ARG//|/ --build-arg }
        BUILD_ARGS="--build-arg $full_str"
        echo -e "${CYAN}[ INFO ] > Build args are the following${BLANK}"
        echo "${BUILD_ARGS}"
    fi
    mkdir -p ${TARGET_DIR}

    if [[ ${COMPRESSION_FORMAT} = "docker-archive" ]]
    then
      echo -e "${CYAN}[ INFO ] > Using docker format build${BLANK}"
      export FORMAT_BUILD=docker
    elif [[ ${COMPRESSION_FORMAT} = "oci-archive" ]]
    then
      echo -e "${CYAN}[ INFO ] > Using oci format build${BLANK}"
      export FORMAT_BUILD=oci
    fi

    if [[ ${GITHUB_API_AUTH} = "true" ]]
    then
      echo -e "${CYAN}[ INFO ] > Authenticating to Github API${BLANK}"
      curl -sSfL -H "Authorization: token ${GITHUB_API_REST_TOKEN}" https://api.github.com
    fi
  script:
  - |-
    set +e
    echo -e "${YELLOW}[ EXECUTING ] > Executing build of ${IMAGE_NAME}:${TAG}.${BLANK}"
    buildah bud --format ${FORMAT_BUILD} \
                --file ${FILE} \
                --tag ${IMAGE_NAME}:${TAG} \
                --tls-verify=${VERIFY_TLS} \
                ${BUILD_ARGS} \
                ${WORKINGDIR}
    if [[ $? = 0 ]]
    then
      echo -e "${GREEN}[ SUCCESS ] > Docker image ${IMAGE_NAME}:${TAG} successfully built with Buildah. ${BLANK}"
      echo -e "${CYAN}[ INFO ] > Displaying buildah built images.${BLANK}"
      buildah images
    else
      echo -e "${RED}[ FAIL ] > Docker image ${IMAGE_NAME}:${TAG} failed to build with Buildah !.${BLANK}"
      exit 1
    fi

    if [[ ${COMPRESS} = "true" ]]
    then
      echo -e "${YELLOW}[ EXECUTING ] > Executing compression of ${IMAGE_NAME}:${TAG}.${BLANK}"
      podman save localhost/${IMAGE_NAME}:${TAG} -o ${TARGET_DIR}/${IMAGE_NAME}.tar --format=${COMPRESSION_FORMAT}
    fi
    if [[ $? = 0 ]]
    then
      size=$(du -sh ${TARGET_DIR}/${IMAGE_NAME}.tar | awk '{print $1}')
      echo -e "${GREEN}[ SUCCESS ] > Docker image ${YELLOW}${IMAGE_NAME}:${TAG}${GREEN} successfully built with Buildah. SIZE --> [ ${PURPLE}${size}${GREEN} ]${BLANK}"
      echo "IMG_TAR_PATH=${TARGET_DIR}/${IMAGE_NAME}.tar" >> buildah-bud.env
      echo "IMG_REF=${IMAGE_NAME}:${TAG}" >> buildah-bud.env
      ls -la ${TARGET_DIR}/
    else
      echo -e "${RED}[ FAIL ] > Docker image ${IMAGE_NAME}:${TAG} failed to build with Buildah !${BLANK}"
      exit 1
    fi
  artifacts:
    reports:
      dotenv: buildah-bud.env
    paths:
    - ${TARGET_DIR}/${IMAGE_NAME}.tar
    expire_in: 15 mins
