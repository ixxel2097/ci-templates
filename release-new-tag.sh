#!/bin/bash
RED="\033[1;31m"
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
BLUE="\033[1;34m"
PURPLE="\033[1;35m"
CYAN="\033[1;36m"
BLANK="\033[0m"
DIR_PIPELINES="./pipelines/*.yaml"
DIR_JOBS="./jobs/*.yaml"
PROJECT="ixxel-pipelines/ci-templates"
TAG=$1

# TAG pipelines
echo -e "${GREEN}[ RUN ] > Changing pipelines ref to $TAG ${BLANK}"
for file in $DIR_PIPELINES
do
    REF=$(yq e '.include[0].ref' $file)
    echo -e "${CYAN}[ INFO ] > File ${YELLOW}$file${CYAN} has currently the ref ${PURPLE}$REF${CYAN} and will be changed to ${PURPLE}$TAG${BLANK}"
    sed -i -E "s#(\\s+ref:).+#\1 $TAG#I" $file
done

# TAG jobs
echo -e "${GREEN}[ RUN ] > Changing jobs ref to $TAG ${BLANK}"
for file in $DIR_JOBS
do
    REF=$(yq e '.include[0].ref' $file)
    echo -e "${CYAN}[ INFO ] > File ${YELLOW}$file${CYAN} has currently the ref ${PURPLE}$REF${CYAN} and will be changed to ${PURPLE}$TAG${BLANK}"
    sed -i -E "s#(\\s+ref:).+#\1 $TAG#I" $file
done
